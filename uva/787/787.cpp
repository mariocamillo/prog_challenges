#include <iostream>

using namespace std;

long long int lmsbrute(int *sequence, int size) {
  if (size == 0) return 0;

  long long int max = sequence[0];
  for (int i = 0; i < size; ++i) {
    long long int mult = 1;
    for (int j = i; j < size; ++j) {
      mult *= sequence[j];
      if (mult > max) max = mult;
    }
  }

  return max;
}

long long int lmsdp(int *sequence, int size) {
  if (size == 1) return sequence[0];

  long long int max = -100000;
  long long int best[100][2];
  int i = 0;
  while (i < size) {
    best[i][0] = best[i][1] = 0;
    if (sequence[i] > 0) {
      best[i][0] = sequence[i];
    } else {
      best[i][1] = sequence[i];
    }

    if (i > 0) {
      for (int j = 0; j < 2; ++j) {
        long long int mult = best[i-1][j]*sequence[i];
        if (mult > 0 && mult > best[i][0]) {
          best[i][0] = mult;
        } else if (mult < 0 && mult < best[i][1]) {
          best[i][1] = mult;
        }
      }
    }

    if (best[i][0] > max) { max = best[i][0]; }
    if (best[i][1] > max) { max = best[i][1]; }
    ++i;
  }

  return max;
}

int main(int argc, char **argv) {
  int sequence[100];

  int i = 0;
  int cur;
  while (cin >> cur) {
    if (cur == -999999) {
      cout << lmsdp(sequence, i) << endl;
      i = 0;
    } else {
      sequence[i++] = cur;
    }
  }

  return 0;
}
