#include <iostream>
#include <set>
#include <array>

using namespace std;

int main(void) {
  array<int, 5>coins {1,5,10,25,50};
  array<long long int, 30050> ways {0};
  ways[0] = 1;
  for (int j = 0; j < 5; ++j) {
    int coin = coins[j];
    for (int i = 1; i < 30001; ++i) {
      if (i >= coin) {
        ways[i] += ways[i-coins[j]];
      }
    }
  }

  int input;
  while (cin >> input) {
    if (ways[input] == 1) {
      cout << "There is only 1 way to produce " << input << " cents change." << endl;
    } else {
      cout << "There are " << ways[input] << " ways to produce " << input << " cents change." << endl;
    }
  }

  return 0;
}
